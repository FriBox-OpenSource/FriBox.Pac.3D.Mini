#FriBox.Pac.3D.Mini

FriBox Pac 3D Mini 开源空气净化器，更小巧，更安静，更专业，更具个性！我要自由的呼吸，Breathe in Free Fresh Fantastic ！FriBox 致力于家庭环境类产品的研发。

盒智环境科技  开源净化器系列：<br>
　FriBox Open Source Project<br>
　Name：FriBox 3D Mini<br>
　Version：1.02<br>
　Date：2017-05-06<br>
　Bom List：<br>
　　　1. USB磁吸电源线 ; <br>
　　　2. 9733涡轮风扇 ; <br>
　　　3. 2档自锁按钮 ; <br>
　　　4. 升压电路模块 ; <br>
　　　5. 内部连接导线 ;<br>
　　　6. 3D打印外壳与内部组件 ;<br>
　　　7. HEPA 或 复合碳HEPA 耗材 ;<br>
<br>
　Home Page： [http://fribox.cn/?page_id=318](http://fribox.cn/?page_id=318)<br>
<br>
<br>
请到附件中下载3D打印文件进行打印，并按照文档进行组装。<br>

![输入图片说明](%E4%B8%89%E8%A7%86%E5%9B%BE.png)
![输入图片说明](%E6%B3%A8%E9%87%8A%E5%9B%BE.png)
![输入图片说明](%E6%AD%A3%E8%A7%86%E5%9B%BE.png)
![输入图片说明](%E8%83%8C%E8%A7%86%E5%9B%BE.png)
![输入图片说明](%E4%BF%AF%E8%A7%86%E5%9B%BE.png)

